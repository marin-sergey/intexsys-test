<?php

namespace App\Observer;

use App\Entity\Entity;
use App\Event\EntityUpdatedEvent;
use App\Manager\EntityManager;

class LoggerObserver implements \SplObserver
{
    /**
     * @var string
     */
    private $logFilePath;

    public function __construct($logFilePath)
    {
        $this->logFilePath = $logFilePath;
    }

    public function update(\SplSubject $subject)
    {
        if ($subject instanceof EntityManager) {
            $event = $subject->getEvent();
            if ($event instanceof EntityUpdatedEvent) {
                // that should be replaced with PSR-3 Logger Interface
                $fp = fopen($this->logFilePath, 'a');

                $logLine = join(' ', [date('Y-m-d H:i:s'), 'entity updated:', $event->getClassName(), json_encode($event->getNewData()), PHP_EOL]);
                fwrite($fp, $logLine);
                fclose($fp);
            }
        }
    }
}
