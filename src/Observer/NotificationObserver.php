<?php

namespace App\Observer;

use App\Event\EntityUpdatedEvent;
use App\Manager\EntityManager;
use App\Service\MailerService;

class NotificationObserver implements \SplObserver
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $qohThreshold;
    private MailerService $mailer;

    public function __construct(MailerService $mailer, $email, $qohThreshold)
    {
        $this->mailer = $mailer;
        $this->email = $email;
        $this->qohThreshold = $qohThreshold;
    }

    /**
     * @param \SplSubject|EntityManager $subject
     * @return void
     */
    public function update(\SplSubject $subject)
    {
        if ($subject instanceof EntityManager) {
            $event = $subject->getEvent();
            if ($event instanceof EntityUpdatedEvent) {

                $old = $event->getOld('qoh');
                $new = $event->getNew('qoh');

                if ($old && $new && $new < $old && $new < $this->qohThreshold) {
                    $this->mailer->send($this->email, 'QOH too small', sprintf('Entity #%s QOH is too small', $event->getNew('sku')));
                }
            }
        }

    }
}
