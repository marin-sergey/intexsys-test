<?php

namespace App\Manager\Store;

interface DataStoreInterface {

    public function set($item, $primary, $data);

    public function get($item, $primary);

    public function delete($item, $primary);

    public function save();

    public function getItemTypes();

    public function getItemKeys($itemType);
}
