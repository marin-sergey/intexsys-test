<?php

//Helper function for printing out error information
/**
 * @return string
 */
function getLastError()
{
    if ($errorInfo = error_get_last()) {
        $errorString = " Error type {$errorInfo['type']}, {$errorInfo['message']} on line {$errorInfo['line']} of " . "{$errorInfo['file']}. ";
    } else {
        $errorString = " Unknown error";
    }

    return $errorString;
}
