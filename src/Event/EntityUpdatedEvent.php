<?php

namespace App\Event;

class EntityUpdatedEvent {

    /**
     * @var array
     */
    private $oldData;
    /**
     * @var array
     */
    private $newData;
    /**
     * @var string
     */
    private $className;

    /**
     * @param string $className
     * @param array $oldData
     * @param array $newData
     */
    public function __construct($className, $oldData, $newData)
    {
        $this->oldData = $oldData;
        $this->newData = $newData;
        $this->className = $className;
    }

    /**
     * @return array
     */
    public function getOldData(): array
    {
        return $this->oldData;
    }

    /**
     * @return array
     */
    public function getNewData(): array
    {
        return $this->newData;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    public function getOld(string $key)
    {
        return $this->oldData[$key] ?? null;
    }

    public function getNew(string $key)
    {
        return $this->newData[$key] ?? null;
    }


}
