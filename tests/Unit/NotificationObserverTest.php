<?php

namespace App\Tests\Unit;

use App\Entity\InventoryItem;
use App\Event\EntityUpdatedEvent;
use App\Manager\EntityManager;
use App\Observer\NotificationObserver;
use App\Service\MailerService;
use PHPUnit\Framework\TestCase;

class NotificationObserverTest extends TestCase
{

    public function testInstantiation()
    {
        $mailer = new MailerService();
        $observer = new NotificationObserver($mailer, 'fake@127.0.0.1', 5);

        $this->assertIsObject($observer);
    }

    public function testEventNoThreshold()
    {
        $mailer = $this->createMock(MailerService::class);
        $mailer->expects($this->never())->method('send');

        $observer = new NotificationObserver($mailer, 'fake@127.0.0.1', 5);

        $emSubject = $this->createMock(EntityManager::class);
        $event = new EntityUpdatedEvent(
            InventoryItem::class,
            ['sku' => 'abc-4589', 'qoh' => 50, 'cost' => '5.67', 'salePrice' => '7.27'],
            ['sku' => 'abc-4589', 'qoh' => 10, 'cost' => '5.10', 'salePrice' => '7.27'],
        );
        $emSubject->expects($this->once())->method('getEvent')->willReturn($event);

        $observer->update($emSubject);
    }

    public function testEventWithThreshold()
    {
        $mailer = $this->createMock(MailerService::class);
        $mailer->expects($this->once())->method('send')->with('fake@127.0.0.1', 'QOH too small', 'Entity #abc-4589 QOH is too small');

        $observer = new NotificationObserver($mailer, 'fake@127.0.0.1', 5);

        $emSubject = $this->createMock(EntityManager::class);
        $event = new EntityUpdatedEvent(
            InventoryItem::class,
            ['sku' => 'abc-4589', 'qoh' => 10, 'cost' => '5.67', 'salePrice' => '7.27'],
            ['sku' => 'abc-4589', 'qoh' => 4, 'cost' => '5.10', 'salePrice' => '7.27'],
        );
        $emSubject->expects($this->once())->method('getEvent')->willReturn($event);

        $observer->update($emSubject);
    }
}
