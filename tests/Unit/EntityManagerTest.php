<?php

namespace App\Tests\Unit;

use App\Entity\Entity;
use App\Manager\EntityManager;
use App\Manager\Store\DataStore;
use App\Manager\Store\DataStoreInterface;
use PHPUnit\Framework\TestCase;

class EntityManagerTest extends TestCase
{
    const DATA_FILE_PATH = __DIR__ . '/../../var/tmp/test-load.store';

    public function testInstantiation()
    {
        $fakeDataStore = new class implements DataStoreInterface {

            public function getItemTypes()
            {
                return [];
            }

            public function getItemKeys($itemType)
            {
                return [];
            }

            public function set($item, $primary, $data)
            {
            }

            public function get($item, $primary)
            {
            }

            public function delete($item, $primary)
            {
            }

            public function save()
            {
            }
        };

        $em = new EntityManager($fakeDataStore);

        $this->assertIsObject($em);
    }

    private function getEntity()
    {
        return new class extends Entity {

            public function getMembers()
            {
                return ["id" => 1, "name" => 1];
            }

            public function getPrimary()
            {
                return "id";
            }
        };
    }

    public function testCreate()
    {
        //public function create($entityName, $data, $fromStore = false)
        $dataStoreMock = $this->createMock(DataStore::class);
        $dataStoreMock->expects($this->once())->method('getItemTypes')->willReturn([]);

        $em = new EntityManager($dataStoreMock);
        $entity = $this->getEntity();

        /** @var Entity $createdEntity */
        $createdEntity = $em->create($entity, ['id' => 1, 'name' => 'Test EM']);

        $this->assertEquals(['id' => 1, 'name' => 'Test EM'], $createdEntity->read());
        $this->assertEquals(1, $createdEntity->id);
        $this->assertEquals('Test EM', $createdEntity->name);
    }

    public function testUpdateData()
    {
        $dataStoreMock = $this->createMock(DataStore::class);
        $dataStoreMock->expects($this->once())->method('getItemTypes')->willReturn([]);

        $em = new EntityManager($dataStoreMock);
        $entity = $this->getEntity();
        $em->create(get_class($entity), ['id' => 2, 'name' => 'Test EM Update']);

        // expected, that object will be updated by reference
        $entityFromEm = $em->findByPrimary(get_class($entity), 2);

        $em->update($entityFromEm, ['id' => 2, 'name' => 'This is updated value']);

        $this->assertEquals('This is updated value', $entityFromEm->name);
    }


    public function testDeleteData()
    {
        $dataStoreMock = $this->createMock(DataStore::class);
        $dataStoreMock->expects($this->once())->method('getItemTypes')->willReturn([]);

        $em = new EntityManager($dataStoreMock);
        $entity = $this->getEntity();

        $em->create(get_class($entity), ['id' => 3, 'name' => 'New entity']);
        $em->create(get_class($entity), ['id' => 4, 'name' => 'Delete Me']);
        $em->create(get_class($entity), ['id' => 5, 'name' => 'Another One']);

        $entityToDelete = $em->findByPrimary(get_class($entity), 4);

        $em->delete($entityToDelete);

        $deletedEntity = $em->findByPrimary(get_class($entity), 4);
        $this->assertNull($deletedEntity);
    }

    public function testUpdateStore()
    {
        $dataStoreMock = $this->createMock(DataStore::class);
        $dataStoreMock->expects($this->once())->method('getItemTypes')->willReturn([]);
        $dataStoreMock->expects($this->exactly(3))->method('set');
        $dataStoreMock->expects($this->once())->method('save');

        $em = new EntityManager($dataStoreMock);
        $entity = $this->getEntity();

        $em->create(get_class($entity), ['id' => 7, 'name' => 'To store 1']);
        $em->create(get_class($entity), ['id' => 8, 'name' => 'To store 2']);
        $em->create(get_class($entity), ['id' => 9, 'name' => 'To store 3']);

        $em->updateStore();
    }

    public function testUpdateObserver()
    {
        $dataStoreMock = $this->createMock(DataStore::class);
        $dataStoreMock->expects($this->once())->method('getItemTypes')->willReturn([]);

        $em = new EntityManager($dataStoreMock);
        $entity = $this->getEntity();

        $observer = $this->createMock(\SplObserver::class);
        $observer->expects($this->once())->method('update')->with($em);
        $em->attach($observer);

        $em->create(get_class($entity), ['id' => 42, 'name' => 'New entity']);
        $entityFromEm = $em->findByPrimary(get_class($entity), 42);
        $em->update($entityFromEm, ['id' => 42, 'name' => 'This is updated value']);
    }
}
