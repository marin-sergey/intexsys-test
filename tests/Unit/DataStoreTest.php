<?php

namespace App\Tests;

use App\Entity\Entity;
use App\Manager\Store\DataStore;
use PHPUnit\Framework\TestCase;

class DataStoreTest extends TestCase {

    const DATA_FILE_PATH = __DIR__ . '/../../var/tmp/test.store';

    public function testInstantiation()
    {
        file_exists(self::DATA_FILE_PATH) && unlink(self::DATA_FILE_PATH);

        $dataStore = new DataStore(self::DATA_FILE_PATH);

        $this->assertIsObject($dataStore);
    }

    private function getDataStore()
    {
        file_exists(self::DATA_FILE_PATH) && unlink(self::DATA_FILE_PATH);

        return new DataStore(self::DATA_FILE_PATH);
    }

    private function getEntity()
    {
        return new class extends Entity {

            public function getMembers()
            {
                return ["id" => 1, "name" => 1];
            }

            public function getPrimary()
            {
                return "id";
            }
        };
    }

    public function testSetAndGet()
    {
        $dataStore = $this->getDataStore();
        $entity = $this->getEntity();

        $dataStore->set(get_class($entity), 1, ['id' => 1, 'name' => 'My Name']);

        $data = $dataStore->get(get_class($entity), 1);

        $this->assertEquals(['id' => 1, 'name' => 'My Name'], $data);
    }

    public function testMultipleSet()
    {
        $dataStore = $this->getDataStore();
        $entity = $this->getEntity();

        $dataStore->set(get_class($entity), 1, ['id' => 1, 'name' => 'First']);
        $dataStore->set(get_class($entity), 2, ['id' => 2, 'name' => 'Second']);
        $dataStore->set(get_class($entity), 3, ['id' => 3, 'name' => 'Third']);

        $keys = $dataStore->getItemKeys(get_class($entity));

        $this->assertEquals([1,2,3], $keys);

        $types = $dataStore->getItemTypes();

        $this->assertContains(get_class($entity), $types);
    }

    public function testDelete()
    {
        $dataStore = $this->getDataStore();
        $entity = $this->getEntity();

        $dataStore->set(get_class($entity), 1, ['id' => 1, 'name' => 'First']);
        $dataStore->set(get_class($entity), 2, ['id' => 2, 'name' => 'Delete me']);
        $dataStore->set(get_class($entity), 3, ['id' => 3, 'name' => 'Third']);

        $dataStore->delete(get_class($entity), 2);

        $entityFromEm = $dataStore->get(get_class($entity), 2);
        $this->assertNull($entityFromEm);

        $keysFromEm = $dataStore->getItemKeys(get_class($entity));
        $this->assertEquals([1,3], $keysFromEm);

        // delete all
        $dataStore->delete(get_class($entity), 1);
        $dataStore->delete(get_class($entity), 3);

        $keysFromEm2 = $dataStore->getItemKeys(get_class($entity));
        $this->assertEquals([], $keysFromEm2);
    }

    public function testFileSaveAndRead()
    {
        $dataStore = $this->getDataStore();
        $entity = $this->getEntity();

        $dataStore->set(get_class($entity), 1, ['id' => 1, 'name' => 'Save me']);
        $dataStore->set(get_class($entity), 2, ['id' => 2, 'name' => 'And me']);
        $dataStore->set(get_class($entity), 3, ['id' => 3, 'name' => 'To file']);

        $dataStore->save();

        $this->assertFileExists(self::DATA_FILE_PATH);

        unset($dataStore);

        $dataStore2 = new DataStore(self::DATA_FILE_PATH);

        $entity1 = $dataStore2->get(get_class($entity), 1);
        $this->assertEquals(['id' => 1, 'name' => 'Save me'], $entity1);

        $keys = $dataStore2->getItemKeys(get_class($entity));
        $this->assertEquals([1,2,3], $keys);
    }

    protected function tearDown(): void
    {
        @unlink(self::DATA_FILE_PATH);
        parent::tearDown();
    }


}
