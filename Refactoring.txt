1. Classes separated by files with respect to PSR-4 autoloading.
2. Added development environment to avoid side effects from tools installed on developer machine.
3. Added phpunit.
4. EntityManages knows too much about storage. $storePath replaced with StorageInterface.
5. fix in getLastError function.
6. Method naming violation: DataStore::get() can be string or null. So, this method better to rename to `find` or replace null result with module related Exception.
7. Added unit-test for DataStore. This class uses root Exceptions. It's better to replace with module related Exception. Fixed file deserialization and save method.
8. Added test for Entity Manager. Fixed delete method. `_id` switched to public (bad solution, caused by strange entity manager design).
9. Added test for NotificationObserver. Refactored event data transfer.

Notes: the design is hideous. SRP violation, Active Record anti-pattern, array based storages instead of real properties in entity classes and etc.


