### Clone it

```shell
git clone https://gitlab.com/marin-sergey/intexsys-test.git
cd intexsys-test
```

Install dependencies

```shell
docker-compose run php composer install
```

Run tests

```shell
docker-compose run php vendor/bin/phpunit
```


Run `driver` function

```shell
docker-compose run php ./bin/driver.php
```
